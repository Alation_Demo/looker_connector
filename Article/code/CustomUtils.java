package com.alation.api.utils.article;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;

import org.json.simple.JSONObject;

import com.alation.api.utils.ApiUtils;

public class CustomUtils {
	
	

	private static final Logger logger = Logger.getLogger(CustomUtils.class);
	// String ArticleURL="/integration/v1/article/";

	/*
	 * To send api call and get customfields response
	 */
	protected static HttpResponse bulkMetaCustomFieldsCall(JSONObject customField) throws Exception {

		String partialUrl = "/api/v1/bulk_metadata/data_dictionary/table?custom_fields="
				+ URLEncoder.encode(customField.toJSONString());
		HttpResponse response = null;

		response = ApiUtils.doGET(partialUrl);

		return response;
	}

	/*
	 * To send api call and get template reponse
	 */
	protected static HttpResponse bulkMetaTemplateCall(String template) throws Exception {
		HttpResponse response = null;
		String partialUrl = "/integration/v1/custom_template/?title=" + URLEncoder.encode(template);

		response = ApiUtils.doGET(partialUrl);
		return response;
	}

	/*
	 * To create bulk-metadata-api's
	 */
	protected static String bulkApiCall(String template, String attributeType, boolean isNewflag) {
		String partialURL = "";

		partialURL = "/api/v1/bulk_metadata/custom_fields/" + template.replaceAll(" ", "%20") + "/" + attributeType
				+ "?create_new=" + isNewflag + "&replace_values=true";
		return partialURL;
	}

	protected static HttpResponse articleAPIforarticleTitle(String articleTitle) throws Exception {
		String partialPath = "/integration/v1/article/?title=" + URLEncoder.encode(articleTitle);
		HttpResponse response = ApiUtils.doGET(partialPath);
		return response;
	}

	protected static HttpResponse tagNameAPI(String tagName, JSONObject tag) throws Exception {
		HttpResponse response = null;
		String partialUrl = "/integration/tag/" + tagName + "/subject/";

		response = ApiUtils.doPOST(new StringEntity(tag.toString()), partialUrl);

		return response;
	}

	protected static HttpResponse CustomTemplatesAPI(String templateName) throws Exception {
		String partialPath;
		if (templateName != null && !templateName.isEmpty()) {
			logger.info("Fetching custom template: " + templateName);
			partialPath = "/integration/v1/custom_template/?title=" + URLEncoder.encode(templateName);
		} else {
			logger.info("Fetching all custom templates");
			partialPath = "/integration/v1/custom_template/";
		}
		HttpResponse response = ApiUtils.doGET(partialPath);
		return response;
	}
	
protected static String regexmatcher(String queryAttribute)
{
	String val="";
	try {
		Pattern regex = Pattern.compile("/(.*?)/");
		Matcher regexMatcher = regex.matcher(queryAttribute);
		while (regexMatcher.find()) {
			val=regexMatcher.group(1).toString();			
			val=val.replaceAll("[^a-zA-Z0-9\\s]", "");
			logger.info(val);
			
		} 
	} catch (Exception ex) {
		// Syntax error in the regular expression
	}
	return val;
}




protected static LinkedHashMap<String,String> regexQuerynamematcher(JSONObject jsonObject)
{
//	String queryname="";
	String jsonobj=jsonObject.toString();
	LinkedHashMap<String,String> matchset = new LinkedHashMap<String,String>();
	try {
		Pattern regex = Pattern.compile("<a.*?article(.*?)>(.*?)<.*?>");
		Matcher regexMatcher = regex.matcher(jsonobj);
		while (regexMatcher.find()) {
			
			String queryarticleid=regexMatcher.group(1).toString().replaceAll("[^0-9]", "");
			String queryarticlename=regexMatcher.group(2).toString();
			matchset.put(queryarticleid,queryarticlename);
		} 
	} catch (Exception ex) {
		// Syntax error in the regular expression
	}
	
	
	return matchset;
}

protected static HttpResponse ArticlesListForCustomTemplatesAPI(List<String> customTemplateId) throws Exception {
	HttpResponse response = null;
	String partialUrl = "/integration/v1/article/?custom_field_templates="+customTemplateId;
	response = ApiUtils.doGET(partialUrl);
	return response;
}


}

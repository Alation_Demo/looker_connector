package com.alation.api.utils.article;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.alation.api.models.AlationArticle;
import com.alation.api.utils.ApiUtils;
import com.alation.api.utils.Constants;
import com.alation.api.utils.PropHelper;
import com.alation.api.utils.files.CSVStoreManager;
import com.fasterxml.jackson.databind.deser.DataFormatReaders.Match;

public class ArticleUtils {

	private static final Logger logger = Logger.getLogger(ArticleUtils.class);
	private static final int HashMap = 0;
	private static Map<String, List<String>> secondaryKeys = new HashMap<>();
	private static Map<String, List<String>> templates = new HashMap<>();
	public static String updatedFileContent = new String();
	static String currentCsvLine  = new String();

	// private List<String> columnNames = new ArrayList<>();
	public static void main(String[] args) {
		// to test createArticle() and getArticleByTitle()
		// String jsonContent = "{\"key\":\"TestArticle5\",
		// \"title\":\"TestArticle5\", \"body\":\"some text to be added\",
		// \"custom_templates\":[55]}";
		// createArticle(jsonContent, "TestArticle5");

		// to test getAllArticles()
		// getAllArticles();

		// to test getCutomTemplateId
		// System.out.println(getCustomTemplateId("Publications"));

		// to test updating with custom_fields
		// String jsonContent1 = "{\"key\":\"TestArticle8\",
		// \"title\":\"TestArticle8\", \"Steward\":[{\"type\": \"user\",
		// \"key\": \"nikhilesh.c@alationdata.com\"}], \"Last
		// Updated\":\"17/12/2018\", \"description\":\"some other text\"}\n" +
		// "{\"key\":\"TestArticle9\", \"title\":\"TestArticle9\",
		// \"Steward\":[{\"type\": \"user\", \"key\":
		// \"nikhilesh.c@alationdata.com\"}], \"Last Updated\":\"17/12/2018\",
		// \"description\":\"some other text\"}";
		// updateArticleWithCustomFields(jsonContent1, "TestArticle5",
		// "Publications");
		// System.out.println(CSVArticleLoader.checkIfArticleExistInAlation("Global
		// Group Party Identifier", "KPI"));
		deleteArticles(1663, 1676);
	}	
	

	public static List<AlationArticle> createArticle(String jsonContent, String articleName) {
		String partialPath = "/integration/v1/article/";
		logger.info("Creating article: " + articleName);
		try {
			HttpResponse response = ApiUtils.doPOST(new StringEntity(jsonContent), partialPath);
			if ((response.getStatusLine().getStatusCode() == 200)
					|| (response.getStatusLine().getStatusCode() == 201)) {
				logger.info("Article " + articleName + " Successfully created");
				return getResponseAsList(response);
			} else {
				logger.error("Error creating article: " + articleName);
				logger.error(new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent())));
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error("Error creating article: " + articleName);
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * creates article if it doesn't exist previously and updates it otherwise
	 *
	 * @param title
	 * @param jsonString
	 * @return
	 */
	public static List<AlationArticle> createOrUpdateArticle(String title, String jsonString) {
		List<AlationArticle> resp = new ArrayList<>();
		List<AlationArticle> alList = getArticlesByTitle(title);

		// Unable to locate an Article
		if (alList.isEmpty()) {
			logger.info("[CREATING] Article: " + title);
			return createArticle(title, jsonString);
		} else {
			for (AlationArticle alationArticle : alList) {
				logger.info("[UPDATING] Article: " + title);
				updateArticleById(jsonString, alationArticle.getId());
			}
			return alList;
		}
	}

	public static List<AlationArticle> getAllArticles() {
		// It can fetch max. of 100 articles only!
		String partialPath = "/integration/v1/article/";
		logger.info("Fetching all articles");
		try {
			HttpResponse response = ApiUtils.doGET(partialPath);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Fetched Articles Succesfully.");
				return getResponseAsList(response);
			} else {
				// TODO: mention the error in a more detailed way by searching
				// for the possible ways of going wrong.
				logger.error("Error fetching articles");
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	public static List<AlationArticle> getArticlesByTitle(String articleTitle) {
		// String partialPath = "/integration/v1/article/?title=" +
		// URLEncoder.encode(articleTitle);
		try {
			// HttpResponse response = ApiUtils.doGET(partialPath);
			HttpResponse response = CustomUtils.articleAPIforarticleTitle(articleTitle);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Fetched Articles Succesfully.");
				return getResponseAsList(response);
			} else {
				// TODO: mention the error in a more detailed way by searching
				// for the possible ways of going wrong.
				logger.error("Error fetching articles");
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	public static List<AlationArticle> getArticleById(long articleId) {
		logger.info("Fetching articles By Id: " + articleId);
		try {
			HttpResponse response = ApiUtils.doGET("/integration/v1/article/" + articleId + "/");
			if (response.getEntity() != null && response.getStatusLine().getStatusCode() != 404) {
				logger.info("Fetched articles By Id: " + articleId);
				return getResponseAsList(response);
			} else {
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	public static List<AlationArticle> getResponseAsList(HttpResponse response) throws IOException, ParseException {
		List<AlationArticle> alationArticles = new ArrayList<>();
		String content = ApiUtils.convert(response.getEntity().getContent());
		JSONParser parser = new JSONParser();
		Object object = parser.parse(content);
		if (object instanceof JSONObject) {
			alationArticles.add(copyJsonToArticle((JSONObject) object));
		} else if (object instanceof JSONArray) {
			JSONArray jsonArray = (JSONArray) object;
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject jsonObject = (JSONObject) jsonArray.get(i);
				alationArticles.add(copyJsonToArticle(jsonObject));
			}
		}
		return alationArticles;
	}

	private static AlationArticle copyJsonToArticle(JSONObject jsonObject) {
		AlationArticle article = new AlationArticle();
		article.setTitle((String) jsonObject.get(Constants.TITLE));
		String authorEmail = (String) ((JSONObject) jsonObject.get(Constants.AUTHOR)).get(Constants.EMAIL);
		article.setAuthorEmail(authorEmail);
		article.setBody((String) jsonObject.get(Constants.BODY));
		article.setId(((Long) (jsonObject.get(Constants.ID))).intValue());
		article.setUrl((String) jsonObject.get(Constants.URL));
		article.setChildren(getChildren((JSONArray) jsonObject.get(Constants.CHILDREN)));
		article.setCustomFields((JSONArray) jsonObject.get(Constants.CUST_FIELDS));
		article.setCustomTemplates((JSONArray) jsonObject.get(Constants.CUST_TEMPLATE));
		return article;
	}

	private static List<AlationArticle> getChildren(JSONArray childrenArray) {
		List<AlationArticle> children = new ArrayList<>();
		for (int i = 0; i < childrenArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) childrenArray.get(i);
			AlationArticle child = new AlationArticle();
			child.setTitle((String) jsonObject.get(Constants.TITLE));
			child.setUrl((String) jsonObject.get(Constants.URL));
			child.setId(((Long) (jsonObject.get(Constants.ID))).intValue());
			children.add(child);
		}
		return children;
	}

	public static List<AlationArticle> updateArticleById(String jsonContent, int articleId) {
		String partialPath = "/integration/v1/article/" + articleId + "/";
		try {
			HttpResponse response = ApiUtils.doPUT(new StringEntity(jsonContent), partialPath);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Article " + articleId + " updated succesfully.");
				return getResponseAsList(response);
			} else {
				// TODO: mention the error in a more detailed way by searching
				// the possible ways of going wrong.
				logger.error("Error updating the article " + articleId);
				logger.error(response.toString());
				logger.error(new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent())));
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return new ArrayList<>();
	}

	public static void updateArticleWithCustomFields(String jsonContent, String articleName, String templateName) {
		logger.info("Updating articles with custom_fields");
		try {
			
			// use %20 for space instead of plus Sign
			String partialPath = "/api/v1/bulk_metadata/custom_fields/" + templateName.replace(" ", "%20")
					+ "/article?create_new=" + PropHelper.getHelper().isCreateNew() + "&replace_values=true";
			HttpResponse response = ApiUtils.doPOST(new StringEntity(jsonContent), partialPath);
			if (response.getStatusLine().getStatusCode() != 200) {
				logger.error("Article " + articleName + " not updated succesfully.");
				logger.error(response.toString());
			} else {
				JSONParser parser = new JSONParser();
				JSONObject responseObject = (JSONObject) parser
						.parse(ApiUtils.convert(response.getEntity().getContent()));
				long number_recieved = (Long) responseObject.get("number_received");
				long new_objects = (Long) responseObject.get("new_objects");
				long updated_objects = (Long) responseObject.get("updated_objects");
				if (((JSONArray) responseObject.get(Constants.ERROR_OBJECTS)).size() > 0
						|| !((String) responseObject.get(Constants.ERROR)).contentEquals("")) {
					logger.error("Error creating/updating article(s)!");
					logger.error("Error_message from server: " + responseObject.get(Constants.ERROR_OBJECTS) + " - "
							+ responseObject.get(Constants.ERROR));
					logger.error(responseObject.toString());
					logger.info(number_recieved + " article(s) sent");
					logger.info(new_objects + " article(s) created");
					logger.info(updated_objects + " article(s) updated");
				} else {
					if (number_recieved == updated_objects) {
						logger.info("All articles updated successfully");
					} else {
						logger.warn(new_objects + " articles doesn't exist in com.alation previously");
						logger.info(new_objects + " article(s) created");
						logger.info(updated_objects + " article(s) updated");
						logger.info(responseObject.toString());
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public static void deleteArticles(int startIndex, int endIndex) {
		for (int i = startIndex; i <= endIndex; i++) {
			// copied from PurgeArticles class
			deleteArticleById(i, "BRUTE_FORCE_DELETE");
		}
	}

	public static void deleteArticleById(int articleId, String description) {
		// Copied this method from purgeArticles
		try {
			logger.info("Deleting : article_id=" + articleId + ")" + ", type=" + description);
			HttpResponse response = null;
			response = ApiUtils.doDELETE("/integration/v1/article/" + articleId + "/");
			if (response.getStatusLine().getStatusCode() == 204) {
				logger.info("Deleted Article: " + articleId + " successfully.");
			} else {
				logger.error(response.toString());
				logger.error("Article: " + articleId + " not deleted!!!");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public static long getCustomTemplateId(String templateName) {
		String partialPath;
		long templateId = -1;
		JSONArray customTemplate = getCustomTemplates(templateName);
		if (customTemplate.size() > 0) {
			JSONObject jsonObject = (JSONObject) customTemplate.get(0);
			templateId = (Long) jsonObject.get(Constants.ID);
			logger.info("template id: " + templateId);
		}
		return templateId;
	}

	public static JSONArray getCustomTemplates(String templateName) {
		JSONArray customTemplates = new JSONArray();
		// String partialPath;
		// if (templateName != null) {
		// logger.info("Fetching custom template: " + templateName);
		// partialPath = "/integration/v1/custom_template/?title=" +
		// URLEncoder.encode(templateName);
		// } else {
		// logger.info("Fetching all custom templates");
		// partialPath = "/integration/v1/custom_template/";
		// }
		try {
			// HttpResponse response = ApiUtils.doGET(partialPath);
			HttpResponse response = CustomUtils.CustomTemplatesAPI(templateName);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Fetching completed.");
				customTemplates = (JSONArray) new JSONParser()
						.parse(ApiUtils.convert(response.getEntity().getContent()));
				if (customTemplates.size() == 0) {
					logger.warn("There are no templates in Alation with name: " + templateName);
				}
			} else {
				logger.error("Error fetching the template(s)!!");
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return customTemplates;
	}

	public static boolean isCustomFieldValidForACustomTemplate(String articleTemplate, String customFieldName)
			throws Exception {
		boolean isValid = false;
		JSONObject customField = new JSONObject();
		JSONArray array = new JSONArray();
		customField.put(customFieldName, array);
		String partialUrl = "/integration/v1/custom_template/?title=" + URLEncoder.encode(articleTemplate);
		// String partialUrl =
		// "/api/v1/bulk_metadata/data_dictionary/"+tableType+"?custom_fields="+
		// URLEncoder.encode(customField.toJSONString());
		HttpResponse response = ApiUtils.doGET(partialUrl);
		if (response.getStatusLine().getStatusCode() == 200) {
			JSONParser pepParser = new JSONParser();
			JSONArray responseArray = (JSONArray) pepParser.parse(ApiUtils.convert(response.getEntity().getContent()));
			for (Object obj : responseArray) {
				JSONObject json = (JSONObject) obj;
				JSONArray fields = (JSONArray) json.get(Constants.FIELDS);
				for (Object object : fields) {
					JSONObject jsonObject = (JSONObject) object;
					if (((String) jsonObject.get(Constants.NAME_SINGULAR)).contentEquals(customFieldName)) {
						isValid = true;
						break;
					} else
						continue;
				}
			}
			// logger.error("This file is skipped!! It contains a custom_field:
			// " + customFieldName +" which doesn't exist in com.alation");
		}
		logger.info("Validating Custom Field: " + customFieldName + " Completed");
		return isValid;
	}

	/**
	 * Check for multiple queries and split the query properly
	 * 
	 * @param query
	 *            Query string with multiple queries
	 * @return Return list of alation query urls
	 */
	private static LinkedHashMap<String, String> parseQuery(String query, String type, String article,
			List<QueryArticle> queryArticleList, String headerName, boolean chilTitleUpdate, String childId,
			String template, String subtitle, String articleid, String subtitleheader) {
		String[] querySplit = query.split("/\\*");
		LinkedHashMap<String, String> queryList = null;
		if (querySplit.length > 0) {
			queryList = new LinkedHashMap<>();
		}
		for (int i = 0; i < querySplit.length; i++) {

			QueryArticle queryArticle = new QueryArticle();
			queryArticle.setHeaderName(headerName);
			queryArticle.setParentArticleTitle(article);			
			queryArticle.setSubtitle(subtitle);
			queryArticle.setSubtitleheader(subtitleheader);
			if (!querySplit[i].isEmpty()) {
				String individualQuery = "/*" + querySplit[i];
				String Queryvalue = CustomUtils.regexmatcher(individualQuery).trim();

				if (!Queryvalue.isEmpty() && Queryvalue != null) {

					String responseUrl = postQuery(individualQuery, Queryvalue.trim(), subtitle, article, queryArticle,
							chilTitleUpdate, childId, articleid);
					queryArticleList.add(queryArticle);

					if (responseUrl != null) {
						queryList.put(responseUrl, Queryvalue);
					}

				}
			}
		}
		logger.info(queryList);
		return queryList;
	}

	/**
	 * Post the query and get the corresponding url
	 * 
	 * @param query
	 *            Query to be posted to alation
	 * @return Query url from the query object
	 */
	protected static String postQuery(String query, String Queryvalue, String type, String article,
			QueryArticle queryArticle, boolean chilTitleUpdate, String childId, String articleid) {
		String responseURL = "";
		try {
			// Long datasourceId = PropHelper.getHelper().getDatasourceId();
			JSONObject jsonObject = new JSONObject();
			String title = article + "-" + type + "-" + Queryvalue;
			List<AlationArticle> response = new ArrayList<AlationArticle>();
			String formatteQuery = "<p>" + "<strong>" + "<pre>" + query + "</pre>" + "</p>" + "</strong>";
			String childtemplate = PropHelper.getHelper().getChildtemplate();
			long templateid = getCustomTemplateId(childtemplate);

			JSONArray array = new JSONArray();
			array.add(templateid);
			jsonObject.put(Constants.TITLE, title);
			jsonObject.put(Constants.BODY, formatteQuery);

			jsonObject.put(Constants.CustTemplate, array);
			if (chilTitleUpdate) {
				response = getArticleById(Long.parseLong(childId));

			} else {
				// response = getArticleById(Long.parseLong(articleid));
				response = getArticlesByTitle(title.trim());
			}
			if (!response.isEmpty() && response.size() == 1) {
				int articleId = response.get(0).getId();
				HttpResponse rep = ApiUtils.doPUT(new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON),
						"/integration/v1/article/" + articleId + "/");

				responseURL = getQueryId(rep, query);
				String Childid = String.valueOf(articleId);
				// updatedFileContent = updatedFileContent.concat("200;;"+
				// Childid+";"+type+";"+query);
				queryArticle.setChildId(Childid);
				queryArticle.setQuery(query);
				

			}

			else {

				HttpResponse resp = ApiUtils.doPOST(
						new StringEntity(jsonObject.toString(), ContentType.APPLICATION_JSON),
						"/integration/v1/article/");

				responseURL = getQueryId(resp, query);
				queryArticle.setChildId(responseURL);
				queryArticle.setQuery(query);

			}

		} catch (Exception e) {
			logger.fatal("Could not find datasource Id", e);
		}
		return responseURL;

	}

	/**
	 * Write success or failure response to CSV file
	 *
	 * @param response
	 *            Http response that we get after posting an API call
	 */
	protected static void writeResponseToCsv(HttpResponse response, JSONObject responseObject) {
		logger.info("Capturing response");
		int status = response.getStatusLine().getStatusCode();
		String errorMessage=new String();
		JSONArray errorObjects;
		if (status == 200 || status == 201) {
			try {

				if (!((String) responseObject.get(Constants.ERROR)).contentEquals("")
						|| (((JSONArray) responseObject.get(Constants.ERROR_OBJECTS)).size() > 0)) {

					 errorObjects = (JSONArray) responseObject.get(Constants.ERROR_OBJECTS);

					logger.error("Error code response 300: " + responseObject.toString());
					
					updatedFileContent = updatedFileContent.concat(300 + ";" + errorObjects.toString() + ";" );
				}
			} catch (Exception e) {
				logger.error(e);
			}
		} else if (status == 400) {
			try {
				String error = (new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent()))).toString()
						.replaceAll(",", " ");
				logger.error("Error code response 400: " + error);
//				updatedFileContent = updatedFileContent
//						.concat(status + ",bad request(" + error + ");" + currentCsvLine);
				updatedFileContent = updatedFileContent.concat(400 + ";" + error + ";" );

			} catch (Exception e) {
				updatedFileContent = updatedFileContent.concat(400 + ";" +",bad request(,"+";" );
			}
		} else if (status == 500 || status == 405) {
			logger.error("Error response 405: " + responseObject.toString());
			updatedFileContent = updatedFileContent.concat(status + ";" + ",Object Not created(" + ";" );
		} else {
			logger.info("Error response 200: " + responseObject.toString());
			updatedFileContent = updatedFileContent
					.concat(status + "," + response.getStatusLine().getReasonPhrase() + ",");
		}
	}

	/**
	 * Build the query string data like html hyperlinks objects by posting all
	 * the queries to alation
	 * 
	 * @param queryAttribute
	 *            Query data
	 * @return query data in alation format
	 */
	private static String buildQueryField(String queryAttribute, String type, String article,
			List<QueryArticle> queryArticleList, String headerName, boolean chilTitleUpdate, String childId,
			String template, String subtitle, String articleid, String subtitleheader) {
		StringBuilder stringBuilder = new StringBuilder();
		String Queryname = type.replaceAll("[^a-zA-Z ]", "").trim();

		try {
			LinkedHashMap<String, String> queryIdsList = parseQuery(queryAttribute, Queryname.trim(), article,
					queryArticleList, headerName, chilTitleUpdate, childId, template, subtitle, articleid,
					subtitleheader);
			for (Map.Entry<String, String> entry : queryIdsList.entrySet()) {
				String csvqueryname = entry.getValue().toString().trim();

				stringBuilder.append("<a href=").append("\"").append(PropHelper.getHelper().getBaseURL()).append("/")
						.append("article").append("/").append(entry.getKey()).append("\"")
						.append(">" + article + "-" + subtitle + "-" + csvqueryname + "</a><br/>")
						.append(System.lineSeparator());

			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return stringBuilder.toString();
	}

	/**
	 * Get related objects information and parse it
	 *
	 * @return
	 */
	private static JSONArray getRelatedObjects(String name, String[] attributes, List<String> columnNames,
			Map<String, List<String>> secondaryKeys) {

		List<String> secondaryObjects = secondaryKeys.get(name);
		JSONObject relatedObject = new JSONObject();
		JSONArray relatedObjectArray = new JSONArray();
		Map<String, List<String>> values = new HashMap<>();
		int maxCount = 1;

		// Loop once to check whether the values are single or multiple. If
		// there are multiple split and store them
		for (String secondaryKey : secondaryObjects) {
			int index = columnNames.indexOf(name + ":" + secondaryKey);
			if (index < attributes.length) {
				String str = attributes[index];
				if (str.contains(",")) {
					str = str.replaceAll("\"", "");
					String[] valuesSplit = str.split(",");
					values.put(secondaryKey, Arrays.asList(valuesSplit));
					if (valuesSplit.length > maxCount) {
						maxCount = valuesSplit.length;
					}
				} else {
					List<String> tempList = new ArrayList<>();
					if (!str.isEmpty()) {
						tempList.add(str);
						values.put(secondaryKey, tempList);
					}
				}
			} else
				return null;
		}

		// The outer FOR loop is for the values which have more than one value
		// provided
		for (int i = 0; i < maxCount; i++) {
			relatedObject = new JSONObject();
			for (String secondaryKey : secondaryObjects) {
				if (secondaryKey.equalsIgnoreCase(Constants.MULTIPICKER)
						|| secondaryKey.equalsIgnoreCase(Constants.MULTISpacePICKER)
						|| secondaryKey.equalsIgnoreCase(Constants.MULTIHYPENPICKER)) {
					List<String> valueList = values.get(secondaryKey);
					if (valueList != null) {
						for (String value : valueList) {
							if (value.contains(",")) {
								String[] valueSplit = value.split(",");
								relatedObjectArray.addAll(Arrays.asList(valueSplit));
							} else {
								relatedObjectArray.add(value);
							}
						}
					}
					return relatedObjectArray;
				} else {
					List<String> valueList = values.get(secondaryKey);
					if (valueList != null && valueList.size() > i) {
						relatedObject.put(secondaryKey, valueList.get(i));
					}
				}
			}
			if (relatedObject.size() == secondaryObjects.size()) {
				relatedObjectArray.add(relatedObject);
			}
		}
		return relatedObjectArray;

	}
	/*
	 * 
	 * @author GPriya JsonArticle is creating json by reading each row of the
	 * CSV file To create json article for posting into alation
	 * 
	 * @param attributes
	 * 
	 * @param columnNames
	 * 
	 * @param keysUsed
	 * 
	 * @param secondaryKeys
	 */

	protected static JSONObject createJsonArticle(String[] attributes, List<String> columnNames, List<String> keysUsed,
			Map<String, List<String>> secondaryKeys, List<QueryArticle> queryArticleList, String articleid,
			String csvchildid, String subtitle, String subtitleheader) throws Exception

	{
		boolean chilTitleUpdate = false;

		String articletitle = "";
		String template = "";

		List<AlationArticle> response = new ArrayList<AlationArticle>();
		if (csvchildid != null && !csvchildid.isEmpty()) {
			chilTitleUpdate = true;
		}

		JSONObject article = new JSONObject();

		// Loop over each columns
		for (String headerName : columnNames) {
			// skip status and error_messages column
			if (headerName.contentEquals(Constants.Status) || headerName.contentEquals(Constants.Error_Messages)) {
				continue;
			}

			articletitle = attributes[columnNames.indexOf(Constants.KEY)];

			if (headerName.contains(":")) {

				String key = headerName.split(":")[0];
				String type = headerName.split(":")[1];
				String q = "";
				// Check for query key and call Query API separately
					if (headerName.contains(type) && headerName.contains(":sub")) {

						try {
							Pattern regex = Pattern.compile("(.*?):(.*?):(.*)");
							Matcher regexMatcher = regex.matcher(headerName);
							while (regexMatcher.find()) {
								for (int i = 1; i <= regexMatcher.groupCount(); i++) {
									q = regexMatcher.group(3);

								}
								logger.info(q);
							}
						} catch (Exception ex) {
							// Syntax error in the regular expression
						}
					
					if (!q.isEmpty() && headerName.contains(q)) {
						LinkedHashMap<String, String> subtitlevalues = new LinkedHashMap<String, String>();
						subtitle = attributes[columnNames.indexOf(headerName)];
						subtitleheader = headerName;
						logger.info(subtitle);
					}

					}
				if (type.equalsIgnoreCase(Constants.QUERYKEY) && !headerName.contains(":sub")) {
					String queryData = buildQueryField(attributes[columnNames.indexOf(headerName)], type, articletitle,
							queryArticleList, headerName, chilTitleUpdate, csvchildid, template, subtitle, articleid,
							subtitleheader);
					article.put(key, queryData);

				} else if (key.equalsIgnoreCase(Constants.QUERYKEY) && !headerName.contains(":sub")) {
					String queryData = buildQueryField(attributes[columnNames.indexOf(headerName)], type, articletitle,
							queryArticleList, headerName, chilTitleUpdate, csvchildid, template, subtitle, articleid,
							subtitleheader);
					article.put(type, queryData);
				} else {

					JSONArray relatedObjects = new JSONArray();

					if (!keysUsed.contains(key)) {
						// JSONArray relatedObjects =new JSONArray();

						relatedObjects = getRelatedObjects(key, attributes, columnNames, secondaryKeys);

						if (relatedObjects != null && !relatedObjects.isEmpty())
							article.put(key, relatedObjects);
						keysUsed.add(key);

					}

				}
			} else {
				// For updating article we need BODY in the request
				if (columnNames.indexOf(headerName) < attributes.length) {
					if (!attributes[columnNames.indexOf(headerName)].isEmpty()) {
						if (Constants.KEY.equalsIgnoreCase(headerName)) {
							article.put(headerName.toLowerCase(), attributes[columnNames.indexOf(headerName)].trim());
						} else {
							article.put(headerName,
									attributes[columnNames.indexOf(headerName)].replaceAll("\u00A0", ""));
						}
					}
				}
			}

		}

		return article;
	}

	/*
	 * To Create Response csv file for update the status
	 */
	protected static Path tocreateReponseFile(String responseFilesLocation, Path filePath) {
		CSVParser csvFileParser = null;
		// to write response into a new file whose name is original file name +
		// _responses
		Path newFilePath = null;
		// checks for responseFileLocation is empty
		// if not, create response file in a location
		// or else create response file in csv file location
		if (!responseFilesLocation.isEmpty()) {
			newFilePath = Paths.get(responseFilesLocation + File.separator
					+ FilenameUtils.getBaseName(filePath.toString()).split("\\.csv")[0] + "_response.csv");
		} else {
			newFilePath = Paths.get(filePath.toString().split("\\.csv")[0] + "_response.csv");
		}
		return newFilePath;
	}

	/*
	 * To get the header columns in the csv file
	 */
	protected static List<String> toGetHeaders(Path filePath, CSVFormat csvFileFormat, Path newFilePath,
			CSVParser csvFileParser) {
		CSVStoreManager csvStoreManager = null;
		List<String> columnNames = new ArrayList<>();
		// Get the header of the file
		Map<String, Integer> headerMap = csvFileParser.getHeaderMap();
		columnNames.addAll(headerMap.keySet());
		logger.info("On Line 1");
		logger.info("CSV Headers: " + columnNames.toString());
		return columnNames;
	}

	/*
	 * To Add Response column headers in the response csv file
	 */
	protected static String[] toAddResponseHeader(List<String> columnNames) {
		String[] columnHeaders = new String[columnNames.size() + 2];
		// To add the column headers
		columnHeaders[0] = Constants.Status;
		columnHeaders[1] = Constants.Error_Messages;
		// columnHeaders[2] = "Hierarchy";
		for (int i = 0; i < columnNames.size(); i++) {
			columnHeaders[i + 2] = columnNames.get(i);
		}
		return columnHeaders;
	}

	/*
	 * To get Secondary keys from the columnvalues
	 */
	protected static Map<String, List<String>> toGetSecondaryKeys(List<String> columnNames) {
		Map<String, List<String>> secondaryKeys = new HashMap<>();
		for (String column : columnNames) {
			if (column.contains(":")) {
				String[] nameSplit = column.split(":");
				List<String> list = new ArrayList<>();
				// Split secondary keys
				if (secondaryKeys.containsKey(nameSplit[0])) {
					list = secondaryKeys.get(nameSplit[0]);
				}
				list.add(nameSplit[1]);
				secondaryKeys.put(nameSplit[0], list);
			}
		}
		return secondaryKeys;
	}

	/*
	 * To check if it is directory To check if it is CSV file
	 */
	protected static void isDirectoryandCheckCSVFile(Path filePath) {
		// Check filepath is directory or anyother file
		if (filePath.toFile().isDirectory()) {
			logger.info("Found Directory: Skipping " + filePath.getFileName());
			return;
		} else if (!filePath.getFileName().toString().toLowerCase().endsWith(Constants.CSV)) {
			logger.info("Found File (non-csv): Skipping " + filePath.getFileName());
			return;
		} else {
			logger.info("Found Valid File (csv):" + filePath.getFileName());
		}
	}

	/*
	 * To add the tags into article
	 */
	protected static boolean toaddingTags(long articleId, String[] tags) {
		boolean flag = false;
		JSONObject tag = new JSONObject();
		tag.put("otype", "article");
		tag.put(Constants.O_ID, articleId);
		for (int i = 0; i < tags.length; i++) {
			String tagName = tags[i];
			tagName = tagName.replaceAll(" ", "%20");
			// String partialUrl = "/integration/tag/" + tagName + "/subject/";
			// To add the tag name into article
			if (!tagName.isEmpty()) {
				logger.info("Adding tag: " + tagName);
				try {
					logger.info("Data posted for Tag: " + tag.toString());
					// HttpResponse response = ApiUtils.doPOST(new
					// StringEntity(tag.toString()), partialUrl);
					HttpResponse response = CustomUtils.tagNameAPI(tagName, tag);
					if (response.getStatusLine().getStatusCode() == 200) {
						logger.info("Tag: " + tagName + " added to " + articleId + " successfully");
						logger.info("Response for tag: "
								+ new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent())));
					} else {
						logger.error("Response for adding tag: " + response.toString());
						JSONObject responseContent = (JSONObject) new JSONParser()
								.parse(ApiUtils.convert(response.getEntity().getContent()));
						updatedFileContent = updatedFileContent.concat(response.getStatusLine().getStatusCode() + ","
								+ responseContent.toString().replaceAll(",", " ") + "," + currentCsvLine
								+ System.lineSeparator());
						logger.error("Error Response for adding tag" + responseContent);
						return false;
					}
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			} else {
				logger.info("Tags not Found");
			}
		}
		return flag;
	}

	/**
	 * // * Get custom template ID by fetching all custom templates and mapping
	 * it // * with custom template name // * // * @param templateName // *
	 * custom template name // * @return Custom template ID //
	 */
	protected static long getCustomTemplateIdforarticle(String templateName) {
		long templateId = -1;
		JSONArray customTemplate = ArticleUtils.getCustomTemplates(templateName);
		if (!customTemplate.isEmpty()) {
			JSONObject jsonObject = (JSONObject) customTemplate.get(0);
			storeCustomFields(jsonObject);
			templateId = (Long) jsonObject.get(Constants.ID);
			logger.info("template id: " + templateId);
		}
		return templateId;
	}

	/**
	 * Associate all the custom fields with the corresponding custom template
	 * and store it in memory for reference
	 *
	 * @param customTemplate
	 *            Custom template
	 */
	private static void storeCustomFields(JSONObject customTemplate) {
		// Map<String, List<String>> templates= new HashMap<>();
		logger.info("Storing the custom fields of the template: " + customTemplate);
		JSONArray fields = (JSONArray) customTemplate.get("fields");
		List<String> customFields = new ArrayList<>();
		// To find SINGULAR values
		for (int i = 0; i < fields.size(); i++) {
			JSONObject field = (JSONObject) fields.get(i);
			customFields.add((String) field.get(Constants.NAME_SINGULAR));
		}
		if (!templates.containsKey(customTemplate.get(Constants.TITLE))) {
			templates.put((String) customTemplate.get(Constants.TITLE), customFields);
		}
	}

	/**
	 * Get all the templates provided in a CSV Row
	 * 
	 * @param csvRecord
	 *            CSV Row as CSVRecord format
	 * @param columnNames
	 *            List of column names
	 * @return Array list of templates
	 * @throws IllegalArgumentException
	 */
	protected static String[] getTemplates(CSVRecord csvRecord, List<String> columnNames)
			throws IllegalArgumentException {
		logger.info("Getting templates from the file");
		Set<String> templateNames = new HashSet<>();
		String[] attributes = new String[csvRecord.size()];
		// Iterate over the column records
		for (int i = 0; i < csvRecord.size(); i++) {
			attributes[i] = csvRecord.get(i);
		}
		// to check template name is there in csv file
		String[] expectedColumnNames = { Constants.templates, Constants.template, Constants.template_names,
				Constants.template_name };
		List<String> expectedNamesList = new ArrayList<>(Arrays.asList(expectedColumnNames));
		// Check template column matches to expected name list.
		int index = getIndexOfTemplatesColumn(columnNames, expectedNamesList);
		String templateName = attributes[index];
		// check template name contains comma
		if (templateName.contains(",")) {
			String[] templates = templateName.replaceAll("\"", "").split(",");
			for (int j = 0; j < templates.length; j++) {
				templateNames.add(templates[j]);
			}
		} else {
			templateNames.add(attributes[index]);
		}
		String[] arr = new String[templateNames.size()];
		return templateNames.toArray(arr);
	}

	/**
	 * Get index of template column from the columns list
	 * 
	 * @param columnNames
	 *            columns names in csv
	 * @param expectedNamesList
	 *            expected mandatory column names
	 * @return Return index of column
	 */
	private static int getIndexOfTemplatesColumn(List<String> columnNames, List<String> expectedNamesList) {
		int index = -1;
		// iterate over to check excpected column values found
		for (String name : expectedNamesList) {
			if (columnNames.contains(name)) {
				index = columnNames.indexOf(name);
			}
		}
		if (index == -1) {
			logger.fatal("Given column names: " + columnNames);
			logger.fatal("Expected any one of the following values: " + expectedNamesList);
			throw new IllegalArgumentException("Expected templates column name doesn't exist in current csv file");
		}
		return index;
	}

	/**
	 * Check whether the row data contains template column
	 * 
	 * @param data
	 *            Row data in JSON object
	 * @return Return the template column header used
	 */
	protected static String getTemplateHeaderColumnValue(JSONObject data) {
		String[] expectedValues = { Constants.templates, Constants.template, Constants.template_names,
				Constants.template_name };
		for (String name : expectedValues) {
			if (data.containsKey(name)) {
				return data.get(name).toString().replaceAll("\"", "");
			}
		}
		return null;
	}

	/**
	 * Get all the fields of a particular custom template
	 * 
	 * @param rowData
	 *            CSV row data in json object
	 * @param template
	 *            tempate name
	 * @return return all fields of the template
	 */
	protected static JSONObject getTemplateSpecificJson(JSONObject rowData, String template) {

		logger.info("Getting custom fields required for the template: " + template);
		JSONObject requiredJson = new JSONObject();
		List<String> customFields = templates.get(template);
		requiredJson.put(Constants.KEY, rowData.get(Constants.KEY));
		requiredJson.put(Constants.TITLE, rowData.get(Constants.TITLE));
		// System.out.println(customFields.get(6));
		// api won't allow to change custom fields and description
		// simultaneously.
		// so not adding it here
		for (Object field : rowData.keySet()) {
			if (customFields.contains(field)) {
				requiredJson.put(field, rowData.get(field));
			}
		}
		return requiredJson;
	}

	/**
	 * Get the entire data of custom templates given the template names as input
	 * 
	 * @param templates
	 *            String Array of template names
	 * @return JSON Array of custom templates data
	 */
	protected static JSONArray getCustomTemplatesInJsonArrayFormat(String[] templates) {
		logger.info("adding custom templates");
		JSONArray customTemplates = new JSONArray();
		for (String template : templates) {
			customTemplates.add(ArticleUtils.getCustomTemplateIdforarticle(template));
		}
		return customTemplates;
	}

	protected static String getQueryId(HttpResponse rep, String query) {
		try {
			if (rep != null) {
				ApiUtils.print(rep);
				JSONObject responseObject = (JSONObject) new JSONParser()
						.parse(ApiUtils.convert(rep.getEntity().getContent()));

				// Get it back - Because Alation does not return the ID's of the
				// newly created objects.
				if (rep.getStatusLine().getStatusCode() == 200 || rep.getStatusLine().getStatusCode() == 201) {
					String error = (String) responseObject.get(Constants.ERROR);
					JSONArray errorObjects = (JSONArray) responseObject.get(Constants.ERROR_OBJECTS);
					if ((error != null && !(error.contentEquals("")))
							|| (errorObjects != null && !errorObjects.isEmpty())) {
						logger.error("Error: " + error + " - " + errorObjects.toString());
						writeResponseToCsv(rep, responseObject);
						return null;
					} else {
						logger.info("Response for " + query + " : " + responseObject.toJSONString());
						return responseObject.get("id").toString();
					}
				} else {
					logger.error(" Unable to create query: " + query + ", Exception: " + rep.toString());
					writeResponseToCsv(rep, responseObject);
					return null;
				}
			}
		} catch (Exception e) {
			logger.fatal("Could not find datasource Id", e);
		}
		return null;
	}

	/**
	 * A separate call to attach the children to the article
	 * 
	 * @param articleId
	 *            Article ID
	 * @param jsonObject
	 *            Article data in json object
	 * @param columnValue
	 *            Children column data
	 * @param tags
	 *            Tags column data
	 * @param customTemplates
	 *            Custom templates array
	 * @return Return true if children are added or else false
	 */
	protected static boolean addChildrenToArticle(long articleId, JSONObject jsonObject, String columnValue,
			String tags, JSONArray customTemplates) {

		columnValue = columnValue.replaceAll("\"", "");
		String[] childrenIds = columnValue.split(";");
		boolean isError = false;
		if (childrenIds == null || childrenIds.length == 0) {

			logger.info("There are no children for " + articleId);
		} else {
			logger.info("Adding children to the article with id: " + articleId);
			JSONObject article = new JSONObject();
			article.put(Constants.KEY, jsonObject.get(Constants.KEY));
			article.put(Constants.TITLE, jsonObject.get(Constants.KEY));
			if (jsonObject.containsKey(Constants.DESC)) {
				article.put(Constants.BODY, jsonObject.get(Constants.DESC));
			} else {
				article.put(Constants.BODY, "");
			}
			JSONArray children = new JSONArray();
			try {
				Set<Long> existingChildrenIds = getExistingChildrenId(articleId);
				for (int i = 0; i < childrenIds.length; i++) {
					if (!childrenIds[i].isEmpty()) {
						logger.info("Adding child: " + childrenIds[i]);

						try {
							long childId = Long.valueOf(childrenIds[i]);
							if (!existingChildrenIds.contains(childId)) {
								// building jsonObject for posting
								JSONObject child = new JSONObject();
								child.put(Constants.OTYPE, Constants.ARTICLE);
								child.put(Constants.ID, childId);
								children.add(child);
							}
						} catch (Exception e) {
							logger.error(e + " - " + e.getMessage());
							isError = true;
						}
					} else {
						logger.info("There are no children for this article");
					}
				}
				String partialUrl = "/integration/v1/article/" + articleId + "/";
				// partialURL=CustomUtils.articleAPIforarticleId(articleId);
				if (!children.isEmpty()) {
					article.put(Constants.CHILDREN, children);
				}
				if (!customTemplates.isEmpty()) {
					article.put(Constants.CUST_TEMPLATE, customTemplates);
				}
				if (!children.isEmpty() || !customTemplates.isEmpty()) {
					try {
						logger.info("Data posted for Article: " + article.toString());
						HttpResponse response = ApiUtils.doPUT(new StringEntity(article.toString()), partialUrl);
						if (response.getStatusLine().getStatusCode() == 200 && !isError) {
							logger.info("Response for Article: "
									+ new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent()))
											.toString().replaceAll(",", " "));
							if (tags != null && !tags.isEmpty()) {
								return addTagsToArticle(articleId, tags);
							} else {
								return true;
							}
						} else if (response.getStatusLine().getStatusCode() == 400) {
							Object responseForChildren = new JSONParser()
									.parse(ApiUtils.convert(response.getEntity().getContent()));
							logger.error("Response for Attaching children: " + responseForChildren.toString());
							updatedFileContent = updatedFileContent.concat("400,"
									+ responseForChildren.toString().replaceAll(",", " ") + "," + currentCsvLine);
						} else {
							if (isError) {
								updatedFileContent = updatedFileContent
										.concat("400,ArticleIds must be numbers (in children) and separated by ';',"
												+ currentCsvLine);
							} else {
								logger.error("Response for Article: " + response.toString());
								JSONObject responseContent = (JSONObject) new JSONParser()
										.parse(ApiUtils.convert(response.getEntity().getContent()));
								updatedFileContent = updatedFileContent.concat(response.getStatusLine().getStatusCode()
										+ ",Error adding children," + currentCsvLine);
								logger.error("Error adding Article: " + responseContent);
							}
						}
					} catch (Exception e) {
						logger.error(e);
					}
				} else {
					if (tags != null && !tags.isEmpty()) {
						return addTagsToArticle(articleId, tags);
					} else {
						updatedFileContent = updatedFileContent.concat("200,," + currentCsvLine);
						return true;
					}
				}
			} catch (NumberFormatException e) {
				logger.error(e + " - " + e.getMessage());
			}
		}
		// else {
		//
		// logger.info("There are no children for " + articleId);
		// }
		return false;
	}

	/**
	 * Add tags to Article
	 *
	 * @param articleId
	 *            articleID
	 * @param columnValue
	 *            column value as string
	 */
	protected static boolean addTagsToArticle(long articleId, String columnValue) {
		logger.info("Adding tags to the article with id: " + articleId);
		columnValue = columnValue.replaceAll("\"", "");
		String[] tags = columnValue.split(",");
		// check tag length not equal to 0
		if (tags != null && tags.length == 0) {
			logger.info("There are no tags for " + articleId);
		} else {
			logger.info("Adding tags to article: " + articleId);
			// building jsonObject for posting
			// To add tags to the article
			boolean addTagResult = ArticleUtils.toaddingTags(articleId, tags);
			logger.info("Adding tags completed");
			return true;
		}
		return true;
	}

	/**
	 * Get existing children IDs from the article
	 * 
	 * @param articleId
	 *            Article ID
	 * @return Set of Child article IDs
	 */
	protected static Set<Long> getExistingChildrenId(long articleId) {
		logger.info("Getting existing children of the article with id: " + articleId);
		List<AlationArticle> articles = ArticleUtils.getArticleById(articleId);
		Set<Long> existingChildrenIds = new HashSet<>();
		if (articles.size() > 0) {
			AlationArticle article = articles.get(0);
			List<AlationArticle> children = article.getChildren();
			for (int i = 0; i < children.size(); i++) {
				existingChildrenIds.add(Long.valueOf(children.get(i).getId()));
			}
		}
		return existingChildrenIds;
	}

	protected static LinkedHashMap<String, String> getarticleId(HttpResponse rep) {
		try {
			LinkedHashMap<String, String> childidandname = new LinkedHashMap<String, String>();
			if (rep != null) {
				ApiUtils.print(rep);
				JSONObject responseObject = (JSONObject) new JSONParser()
						.parse(ApiUtils.convert(rep.getEntity().getContent()));

				// Get it back - Because Alation does not return the ID's of the
				// newly created objects.
				if (rep.getStatusLine().getStatusCode() == 200 || rep.getStatusLine().getStatusCode() == 201) {
					String error = (String) responseObject.get(Constants.ERROR);
					JSONArray errorObjects = (JSONArray) responseObject.get(Constants.ERROR_OBJECTS);
					if ((error != null && !(error.contentEquals("")))
							|| (errorObjects != null && !errorObjects.isEmpty())) {
						logger.error("Error: " + error + " - " + errorObjects.toString());
						writeResponseToCsv(rep, responseObject);
						return null;
					} else {
						logger.info("Response for  articleid : " + responseObject.toJSONString());
						logger.info(responseObject.get("title").toString());

						JSONArray templateArray = (JSONArray) responseObject.get("custom_templates");
						JSONObject customtemplate = (JSONObject) templateArray.get(0);
						childidandname.put(responseObject.get("title").toString(),
								customtemplate.get("title").toString());
						return childidandname;

					}
				} else {
					logger.error(" Unable to get articleid, Exception: " + rep.toString());
					writeResponseToCsv(rep, responseObject);
					return null;
				}
			}
		} catch (Exception e) {
			logger.fatal("Could not find datasource Id", e);
		}
		return null;
	}

	protected static LinkedHashMap<String, String> getparentresponse(String parentid) throws Exception {
		HttpResponse rep;
		rep = ApiUtils.doGET("/integration/v1/article/" + parentid + "/");
		LinkedHashMap<String, String> article_Name = new LinkedHashMap<String, String>();
		article_Name = getarticleId(rep);
		return article_Name;
	}

	protected static void updateChildInParent(String parentid,String headername) throws Exception {

		HttpResponse rep;
		rep = ApiUtils.doGET("/integration/v1/article/" + parentid + "/");

		String customfieldData = "";
		StringBuilder stringBuilder = new StringBuilder();

		JSONObject parentArticle = (JSONObject) new JSONParser().parse(ApiUtils.convert(rep.getEntity().getContent()));
		String title = parentArticle.get(Constants.TITLE).toString();
		JSONArray customTemplates = (JSONArray) parentArticle.get(Constants.CUST_TEMPLATE);
		String templateName = ((JSONObject) customTemplates.get(0)).get(Constants.TITLE).toString();

		JSONArray childArticles = (JSONArray) parentArticle.get(Constants.CHILDREN);
		JSONArray custom_fields = (JSONArray) parentArticle.get(Constants.CUST_FIELDS);

		for (int i = 0; i < custom_fields.size(); i++) {

			JSONObject custom_field = (JSONObject) custom_fields.get(i);
			String fieldName = custom_field.get(Constants.FIELD_NAME).toString();
			if (fieldName.equalsIgnoreCase(headername)) {
				String field_name=fieldName;
				customfieldData = custom_field.get(Constants.VALUE).toString();
				for (int j = 0; j < childArticles.size(); j++) {
					JSONObject childArticle = (JSONObject) childArticles.get(j);
					String id = childArticle.get(Constants.ID).toString();
					String childTitle = childArticle.get(Constants.TITLE).toString();
					if (customfieldData.contains("/" + id)) {

						stringBuilder.append("<a href=").append("\"").append(PropHelper.getHelper().getBaseURL())
								.append("/").append("article").append("/").append(id).append("\"")
								.append(">" + childTitle + "</a><br/>").append(System.lineSeparator());
					}
					
				}
				JSONObject jsonObject = new JSONObject();
				jsonObject.put(Constants.KEY,title);
				jsonObject.put(field_name,stringBuilder.toString());
				

				updateArticleWithCustomFields(jsonObject.toString(), title, templateName);
				break;
			}
		}
		return;
	}

	
/*	public static List<AlationArticle> getAllcustomTeplates() {
		// It can fetch max. of 100 articles only!
		String partialPath = "/integration/v1/custom_template";
		logger.info("Fetching all custom templates");
		try {
			HttpResponse response = ApiUtils.doGET(partialPath);
			if (response.getStatusLine().getStatusCode() == 200) {
				logger.info("Fetched custom_templates Succesfully.");
				System.out.println(response);
				return getResponseAsList(response);
			} else {
				// TODO: mention the error in a more detailed way by searching
				// for the possible ways of going wrong.
				logger.error("Error fetching Custom Templates");
				logger.error(response.toString());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	*/
	
	/*protected static Path tocreateExportFile(String responseFilesLocation) {
		CSVParser csvFileParser = null;
		// to write response into a new file whose name is original file name +
		// _responses
		Path newFilePath = null;
		// checks for responseFileLocation is empty
		// if not, create response file in a location
		// or else create response file in csv file location
		if (!responseFilesLocation.isEmpty()) {
			newFilePath = Paths.get(responseFilesLocation + File.separator
					+ FilenameUtils.getBaseName(filePath.toString()).split("\\.csv")[0] + "_response.csv");
		} else {
			newFilePath = Paths.get(filePath.toString().split("\\.csv")[0] + "_response.csv");
		}
		return newFilePath;
	}
*/	
	
	protected static Path tocreateExportFile(String exportFilesLocation , String title) {
		CSVParser csvFileParser = null;
		// to write response into a new file whose name is original file name +
		// _responses
		Path newFilePath = null;
		// checks for responseFileLocation is empty
		// if not, create response file in a location
		// or else create response file in csv file location
		if (!exportFilesLocation.isEmpty()) {
			newFilePath = Paths.get(exportFilesLocation + File.separator
					+ title + "_export.csv");
		} else {
			//newFilePath = Paths.get(filePath.toString().split("\\.csv")[0] + "_response.csv");
		}
		return newFilePath;
	}
	

	public static JSONArray getArticlesListByCustomTemplateId(List<String> customTemplateId) throws Exception {
		HttpResponse response = CustomUtils.ArticlesListForCustomTemplatesAPI(customTemplateId);
		JSONArray articleList = (JSONArray) new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent()));
		return articleList;
	}
}
